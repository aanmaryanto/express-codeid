// const initdb = require('../config/db.config')
const dotenv = require('dotenv').config()
const mongoClient = require('mongodb').MongoClient
const redis = require('redis');
const client = redis.createClient();
const jwt = require('jsonwebtoken');

const UsersController = {

    getToken(req, res) {
        // example: {"username":"ADMIN","password":"PASSWORD"}
        console.log("getToken ")
        var data = req.body
        var username = data['username']
        var password = data['password']
        if(username == process.env.USER && password == process.env.PASSWORD) {
            var token = jwt.sign({
                data: '1n!StrIn9'
            }, process.env.SALT, {expiresIn: '1h'})
            res.send({"token" : token})
        }else{
            res.send({"message":"combination username and password wrong"})
        }
    },

    users_all(req, res) {
        console.log("users.controller.js -> get all users")
        mongoClient.connect(process.env.MONGODB_URL, function(err, db){
            if(err) {
                console.log("users_all : "+err)
                throw err
            }
            db.db('aan_maryanto').collection('users').find({}).toArray(function(err, respon){
                if(err) {
                    console.log("users_all : "+err)
                    throw err
                }else{
                    res.send(respon)
                }
                db.close()
            });
        })
    },

    users_by_id(req, res) {
        // example: {"Id":"08"}
        console.log("users.controller.js -> get users by id")
        var data = req.body
        console.log("Body : "+JSON.stringify(data))
        mongoClient.connect(process.env.MONGODB_URL, function(err, db) {
            if(err) {
                console.log("users_by_id : "+err)
            }
            db.db('aan_maryanto').collection('users').findOne(data, function(err, respon){
                if(err) {
                    console.log("users_by_id : "+err)
                    throw err
                }else{
                    res.send(respon)
                }
                db.close()
            })
        })
    },

    users_by_accountNumber(req, res) {
        // example: {"accountNumber":"9878"}
        console.log("users.controller.js -> get users by accountNumber")
        var data = req.body
        console.log("Body : "+JSON.stringify(data))
        mongoClient.connect(process.env.MONGODB_URL, function(err, db) {
            if(err) {
                console.log("users_by_id : "+err)
            }
            db.db('aan_maryanto').collection('users').findOne(data, function(err, respon){
                if(err) {
                    console.log("users_by_accountNumber : "+err)
                    throw err
                }else{
                    res.send(respon)
                }
                db.close()
            })
        })
    },

    users_insert(req, res) {
        //example:  
        //{
        //     "Id": "08",
        //     "userName": "kajka",
        //     "accountNumber": "9878",
        //     "emailAddress": "maryanto.aan@gmail.com",
        //     "identityNumber": "9899"
        // }
        console.log("users.controller.js -> insert users")
        var data = req.body
        console.log("Body : "+JSON.stringify(data))
        mongoClient.connect(process.env.MONGODB_URL, function(err, db) {
            if(err) {
                console.log("users_insert : "+err)
                throw err
            }
            db.db('aan_maryanto').collection('users').insertOne(data, function(err, respon) {
                if(err){
                    console.log("users_insert : "+err)
                    throw err
                }else{
                    client.on('error', function(err){
                        console.log("Redis :"+error)
                    })
                    client.set(data['Id'], JSON.stringify(data), redis.print)
                    res.send(respon)

                }
                db.close() 
            })
        })
    },

    users_update(req, res) {
        // Example
        // {
        //     "param":{
        //         "Id":"10"
        //     },
        //     "value":{
        //         "Id": "10",
        //         "userName": "9989",
        //         "accountNumber": "9878",
        //         "emailAddress": "maryanto.aan@gmail.com",
        //         "identityNumber": "1000"
        //     }
        // }
        console.log("users.controller.js -> update users")
        var param = req.body['param']
        var value = req.body['value']
        console.log("Body : "+JSON.stringify(req.body))
        mongoClient.connect(process.env.MONGODB_URL, function(err, db) {
            if(err) {
                console.log("users_update : "+err)
                throw err
            }

            db.db('aan_maryanto').collection('users').updateOne(param, {$set:value}, function(err, respon) {
                if(err) {
                    console.log("users_update : "+err)
                    throw err
                }else{
                    client.on('error', function(err) {
                        console.log("Redis : "+error)
                    })
                    client.set(value['Id'], JSON.stringify(value), redis.print)
                    res.send(respon)
                }
                db.close()
            })
        })
    },

    users_delete(req, res) {
        // example : {
        //     "Id": "10"
        // }
        console.log("users.controller.js -> delete users")
        var data = req.body
        console.log("Body : "+ JSON.stringify(data))
        mongoClient.connect(process.env.MONGODB_URL, function(err, db) {
            if(err) {
                console.log("users_delete : "+err)
                throw err
            }
            db.db('aan_maryanto').collection('users').deleteOne(data, function(err, respon){
                if(err){
                    console.log("users_delete : "+ err)
                    throw err
                }else{
                    client.del(data['Id'])
                    res.send(respon)
                }
                db.close()
            })
        })
    }
}

module.exports = UsersController;