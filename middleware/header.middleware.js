var dotenv = require('dotenv').config();
const jwt = require('jsonwebtoken');

const checkHeader = (req, res, next) => {
    console.log("Check Header")
    if(!req.get('TOKEN-API')) {
        res.send({"message":"Header is required"});
    }else{
        jwt.verify(req.get('TOKEN-API'), process.env.SALT, (err, decoded) => {
            if(err) {
                res.send(err)
                throw err
            }
            next()
        })
    }
}

module.exports = checkHeader;
