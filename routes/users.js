var express = require('express');
var router = express.Router();
var headerMiddleware = require('../middleware/header.middleware');
var usersController = require('../controller/users.controller');

/* GET users listing. */
router.get('/all', headerMiddleware, usersController.users_all);

router.post('/byid', headerMiddleware, usersController.users_by_id);

router.post('/byaccountnumber', headerMiddleware, usersController.users_by_accountNumber);

router.post('/users-insert', headerMiddleware, usersController.users_insert);

router.post('/users-update', headerMiddleware, usersController.users_update);

router.post('/users-delete', headerMiddleware, usersController.users_delete);

module.exports = router;
