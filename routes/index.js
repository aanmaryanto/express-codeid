var express = require('express');
var router = express.Router();
var usersController = require('../controller/users.controller')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Aan Maryanto Microservice' });
});

router.post('/getToken', usersController.getToken);

module.exports = router;
